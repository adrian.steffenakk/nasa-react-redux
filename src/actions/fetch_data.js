import axios from 'axios';

export default function fetchData() {
    const API_KEY = 'd01o62K6BHHkUDtGgYpFwxUWPl5kIZUIK6smbu3E'; // API KEY
    const END_POINT = 'https://api.nasa.gov/planetary/apod?api_key='; // API Endpoint
    const request = axios.get(END_POINT+API_KEY); // GET Request

    return {
        type: 'FETCH_DATA', // Action Type
        payload: request // Action Payload (cargo)
    };
}